package day2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class Google {

	public static void main(String[] args) {
		
		
	System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
	
		driver.get("http://www.google.com");
		
		String title = driver.getTitle();
		System.out.println("title of the page is : "+ title);
		
		driver.quit();
		

	}

}
